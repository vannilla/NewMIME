// NewMIME - Add a new MIME entry to mimeType.rdf
// Copyright (C) 2018-2025  Alessio Vanni

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

var NewMIME = (function () {
    "use strict";
    Components.utils.import("resource://gre/modules/FileUtils.jsm");
    let CC = Components.classes;
    let CI = Components.interfaces;

    let Strings = document.getElementById("newmime-strings");
    
    let o = {};

    o.openWindow = function () {
	window.open("chrome://newmime/content/window.xul",
		    "NewMIME", "chrome,centerwindow");
    };

    o.operate = function () {
	/* Modules */
	let Prompt = CC["@mozilla.org/embedcomp/prompt-service;1"]
	.getService(CI.nsIPromptService);
	let inputstream =  CC["@mozilla.org/network/file-input-stream;1"]
	    .createInstance(CI.nsIFileInputStream);
	let input = CC["@mozilla.org/intl/converter-input-stream;1"]
	    .createInstance(CI.nsIConverterInputStream);
	let outputstream = CC["@mozilla.org/network/file-output-stream;1"]
	    .createInstance(CI.nsIFileOutputStream);
	let output = CC["@mozilla.org/intl/converter-output-stream;1"]
	    .createInstance(CI.nsIConverterOutputStream);
	let Directory = CC["@mozilla.org/file/directory_service;1"]
	    .getService(CI.nsIProperties);

	/* Namespaces */
	let RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	let NC = "http://home.netscape.com/NC-rdf#";

	/* Code: */
	let mime = document.getElementById("newmime-input-box").value;
	if (mime.length == 0) {
	    Prompt.alert(window,
			 Strings.getString("error"),
			 Strings.getString("invalidInput"));
	    throw Strings.getString("invalidInput");
	}

	if (mime.split("/").length != 2) {
	    Prompt.alert(window,
			 Strings.getString("error"),
			 Strings.getString("invalidInput"));
	    throw Strings.getFormattedString("invalidInputF", [mime]);
	}
	
	let file = FileUtils.getFile("ProfD", ["mimeTypes.rdf"]);
	if (file.exists() == false) {
	    Prompt.alert(window,
			 Strings.getString("error"),
			 Strings.getFormattedString("noFile", ["mimeTypes.rdf"]));
	    throw Strings.getFormattedString("noFile", ["mimeTypes.rdf"]);
	}

	inputstream.init(file, -1, 0, 0);
	input.init(inputstream, "UTF-8", 0, 0);

	let data = "";
	let str = {};
	let read = 0;
	
	do {
	    read = input.readString(1024, str);
	    data = data+str.value;
	} while (read != 0);

	input.close();
	inputstream.close();

	let parser = new DOMParser();
	let mimeTypes = parser.parseFromString(data, "text/xml");
	if (mimeTypes.getElementsByTagName('parsererror').length > 0) {
	    throw Strings.getString("parseError")
	}

	let seqs = mimeTypes.getElementsByTagNameNS(RDF, "Seq");

	let index = -1;
	for (let i=0; i<seqs.length && index<0; ++i) {
	    if (seqs[i].getAttributeNS(RDF, "about") == "urn:mimetypes:root") {
		index = i;
	    }
	}

	if (index < 0) {
	    Prompt.alert(window,
			 Strings.getString("error"),
			 Strings.getString("noSeq"));
	    throw "Can't find MIME Seq element!";
	}

	if (seqs[index].children.length > 0) {
	    let c = seqs[index].children;
	    for (let i=0; i<c.length; ++i) {
		if (c[i].getAttributeNS(RDF, "resource")
		    == "urn:mimetype:"+mime) {
		    Prompt.alert(window,
				 Strings.getString("error"),
				 Strings.getFormattedString("duplicate", [mime]));
		    throw Strings.getFormattedString("duplicate", [mime]);
		}
	    }
	}

	let newseq = mimeTypes.createElementNS(RDF, "RDF:li");
	newseq.setAttributeNS(RDF, "RDF:resource", "urn:mimetype:"+mime);
	seqs[index].appendChild(newseq);

	let description = document.getElementById("newmime-desc-box").value;
	let newdesc = mimeTypes.createElementNS(RDF, "RDF:Description");
	newdesc.setAttributeNS(RDF, "RDF:about", "urn:mimetype:"+mime);
	newdesc.setAttributeNS(NC, "NC:value", mime);
	newdesc.setAttributeNS(NC, "NC:editable", "true");
	newdesc.setAttributeNS(NC, "NC:description", description);
	mimeTypes.documentElement.appendChild(newdesc);

	file = FileUtils.getFile("TmpD", ["mimeTypes.rdf.tmp"]);
	outputstream.init(file, -1, -1, 0);
	output.init(outputstream, "UTF-8", 0, 0);

	let serial = new XMLSerializer();
	output.writeString(serial.serializeToString(mimeTypes));

	output.close();
	outputstream.close();

	file.moveTo(Directory.get("ProfD", CI.nsIFile), "mimeTypes.rdf");

	Prompt.alert(window,
		     Strings.getString("success"),
		     Strings.getString("done"));
    };

    return o;
})();
